import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class TestTemperature {

    public static List<String> getTemperature() {
        List<String> temp = new ArrayList<>();
        WebDriver driver = DriverFactory.getDriver();
        try {
            driver.get("https://www.windy.com/?44.609,33.530,5");
            driver.findElement(By.xpath("//*[text()='Sevastopol']")).click();
            WebElement tempTable = driver.findElement(By.className("td-temp"));
            List<WebElement> elements = tempTable.findElements(By.tagName("td"));
            for (WebElement e : elements) {
                temp.add(e.getText());
                if (e.getAttribute("class").equals("day-end"))
                    break;
            }
        } finally {
            driver.quit();
        }
        return temp;
    }

}
